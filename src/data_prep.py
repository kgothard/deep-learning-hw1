# set the matplotlib backend so figures can be saved in the background
import matplotlib
import joblib
#matplotlib.use("Agg")
 
# import the necessary packages
#from pyimagesearch.resnet import ResNet
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from keras.utils import np_utils
import numpy as np
import cv2
import os
import glob

def get_paths(sample):
#    data_dir = '~deep-learning-hw1/png/*'
    data_dir = '../sketches_png/*'
    imagePaths = []
    cat_dir_list = glob.glob(data_dir)
    if sample:
        cat_dir_list = cat_dir_list[:sample]
    for cat_dir in cat_dir_list:
        catImagePaths = glob.glob(cat_dir+'/*')
        if len(catImagePaths) != 0:
            imagePaths += catImagePaths
    return imagePaths

def encode_labels(labels):
    code = np.array(labels)
    label_encoder = LabelEncoder()
    vec = label_encoder.fit_transform(code)
    num_classes = len(set(labels))
    labels = np_utils.to_categorical(vec, num_classes)
    
    return labels

def get_data(sample = None):
    data = []
    labels = []
    imagePaths = get_paths(sample)

    for imagePath in imagePaths:
        label = imagePath.split(os.path.sep)[-2]
        image = cv2.imread(imagePath)
        image = cv2.resize(image, (224, 224))
        data.append(image)
        labels.append(label)
    num_classes = len(set(labels))
    print(num_classes)
    data = np.array(data, dtype="float") / 255.0
    labels = encode_labels(labels)
    
    return data, labels


def main():
    # data, labels = get_data(20)

    data = joblib.load('data.joblib')
    labels = joblib.load('labels.joblib')

    x_train, x_test, y_train, y_test = train_test_split(data, labels, test_size=0.25, random_state=42)

    joblib.dump(x_train, 'train_data.joblib')
    joblib.dump(x_test, 'test_data.joblib')

    joblib.dump(y_train, 'train_labels.joblib')
    joblib.dump(y_test, 'test_labels.joblib')
    
if __name__=="__main__":
    main() 