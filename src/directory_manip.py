import os
import glob

test_images = []
categories = []
for directory in glob.glob('../png/*'):
    categories.append(directory.split('/')[-1])
    images = glob.glob(directory+'/*')
    test_images += images[60:]
    
categories.remove('filelist.txt')
    
os.system('mkdir ../test')

for cat in categories:
    os.system('mkdir \"../test/'+cat+'\"')

for image in test_images:
    cat = image.split('/')[-2]
    try:
        os.system('mv \"'+image+'\" \"../test/'+cat+'\"')
    except:
        print(image)
