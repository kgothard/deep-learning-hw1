import matplotlib.pyplot as plt
import numpy as np

from tensorflow.python.client import device_lib

import keras
from keras.models import Sequential
from keras.layers import Dense, Flatten, Conv2D, MaxPool2D
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split

import joblib

np.random.seed(1000)

def main(
        batch_size=256,
        epochs=3):
    
    NUM_CLASSES = 250
    
    ################################# Gather the data #################################
    
    data = joblib.load('data.joblib')
    labels = joblib.load('labels.joblib')
        
    x_train, x_test, y_train, y_test = train_test_split(data, labels, test_size=0.25, random_state=42)

    print(x_train.shape)
    print(y_train.shape)
    
    ################################# Build the model #################################
    
    opt = Adam(lr=0.01)
    model = build_vgg16(NUM_CLASSES)
    model.compile(
        optimizer=opt,
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    model.summary()
    
    ################################# Parallelize #################################

    # Distribute the neural network over multiple GPUs if available.
    gpu_count = len(available_gpus())

    if gpu_count > 1:
        print(f"\n\nModel parallelized over {gpu_count} GPUs.\n\n")
        parallel_model = keras.utils.multi_gpu_model(model, gpus=gpu_count)
    else:
        print("\n\nModel not parallelized over GPUs.\n\n")
        parallel_model = model

    parallel_model.compile(
        optimizer=opt,
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    
    ################################# Fit the model #################################
    
    checkpoint = keras.callbacks.ModelCheckpoint(
        '../output/chkpt_fromscratch_weights.h5',
        monitor='val_acc',
        save_weights_only=True,
        save_best_only=True,
    )
    
    history = parallel_model.fit(
        x_train,
        y_train,
        validation_split = 0.1,
        batch_size=batch_size,
        epochs=epochs,
        verbose=1,
        callbacks=[checkpoint],
    )
    
    # Save the model
    model_json = parallel_model.to_json()
    with open("scratch_vgg16.json", "w") as json_file:
        json_file.write(model_json)
    model.save('scratch_vgg16.h5')
    
    score = parallel_model.evaluate(x_test, y_test, verbose=1, batch_size=batch_size)

    print(f'Test score:    {score[0]: .4f}')
    print(f'Test accuracy: {score[1] * 100.:.2f}')   
    
    plot_acc(history)
    
    ##############################################################################
    

def build_vgg16(num_classes):
    model = Sequential()
    model.add(Conv2D(input_shape=(224,224,3),filters=64,kernel_size=(3,3),padding="same", activation="relu"))
    model.add(Conv2D(filters=64,kernel_size=(3,3),padding="same", activation="relu"))
    model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
    model.add(Conv2D(filters=128, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(Conv2D(filters=128, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
    model.add(Conv2D(filters=256, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(Conv2D(filters=256, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(Conv2D(filters=256, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
    model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
    model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
    model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
    model.add(Flatten())
    model.add(Dense(units=4096,activation="relu"))
    model.add(Dense(units=4096,activation="relu"))
    model.add(Dense(units=num_classes, activation="softmax"))

    return model

def available_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']

def plot_acc(history):
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig('scratch_acc.png')

if __name__ == '__main__':
    main(epochs=4)
