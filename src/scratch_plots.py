import itertools

import matplotlib.pyplot as plt
import numpy as np

from sklearn.metrics import confusion_matrix, classification_report

from tensorflow.python.client import device_lib

import keras
from keras.models import Sequential
from keras.layers import Dense, Flatten, Conv2D, MaxPool2D
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.models import model_from_json

import joblib

import warnings
warnings.filterwarnings("ignore")

np.random.seed(1000)

def main(
        batch_size=256,
        epochs=3):
    
    NUM_CLASSES = 250
    
    ################################# Gather the data #################################
    
    x_test = joblib.load('test_data.joblib')
    y_test = joblib.load('test_labels.joblib')

    print(x_test.shape)
    print(y_test.shape)
    
    ################################# Build the model #################################
    
    # load json and create model
    json_file = open('../scratch_vgg16.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights("../scratch_vgg16.h5")
    print("Loaded model from disk")
    
    opt = Adam(lr=0.01)    
    
    ################################# Parallelize #################################

    # Distribute the neural network over multiple GPUs if available.
    gpu_count = len(available_gpus())

    if gpu_count > 1:
        print(f"\n\nModel parallelized over {gpu_count} GPUs.\n\n")
        parallel_model = keras.utils.multi_gpu_model(model, gpus=gpu_count)
    else:
        print("\n\nModel not parallelized over GPUs.\n\n")
        parallel_model = model

    parallel_model.compile(
        optimizer=opt,
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    
    print(parallel_model.summary())
    
    ##############################################################################
    
    preds = parallel_model.predict(x_test, batch_size=batch_size)

    c = confusion_matrix(np.argmax(y_test, axis=-1), np.argmax(preds, axis=-1))
    plot_confusion_matrix(
        c,
        list(range(NUM_CLASSES)),
        normalize=False,
        output_path='../output',
    )
    plot_confusion_matrix(
        c,
        list(range(NUM_CLASSES)),
        normalize=True,
        output_path='../output',
    )

    print(
        classification_report(
            np.argmax(y_test, axis=-1),
            np.argmax(preds, axis=-1),
            target_names=[str(x) for x in range(NUM_CLASSES)],
        )
    )


def available_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']

def plot_acc(history):
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.show()

def plot_confusion_matrix(
        cm,
        classes,
        normalize=False,
        title='Confusion matrix',
        cmap='Blues',
        output_path='.',
):
    """
    Logs and plots a confusion matrix, e.g. text and image output.

    Adapted from:
        http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        tag = '_norm'
        print("Normalized confusion matrix:")
    else:
        tag = ''
        print('Confusion matrix:')
    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=plt.get_cmap(cmap))
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(
            j,
            i,
            format(cm[i, j], fmt),
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black",
        )
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.savefig(f'{output_path}/scratch_confusion{tag}.png')
    plt.close()


if __name__ == '__main__':
    main(epochs=4)
