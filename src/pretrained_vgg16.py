import keras
from keras.applications import VGG16
from keras.optimizers import Adam
from tensorflow.python.client import device_lib
from keras.layers import Dense
from keras.models import Model
from sklearn.model_selection import train_test_split

import joblib

import matplotlib.pyplot as plt
    

def main(batch_size=256, epochs=3):
        
    NUM_CLASSES = 250
    
#    ################################# Gather the data #################################

    data = joblib.load('data.joblib')
    labels = joblib.load('labels.joblib')
        
    x_train, x_test, y_train, y_test = train_test_split(data, labels, test_size=0.25, random_state=42)
    print(x_train.shape)
    print(y_train.shape)
    
    ################################# Build the model #################################

    # Acquire the base model - VGG16 trained on ImageNet data
    base_model = VGG16(weights='imagenet')
    
    model = build_finetune_model(base_model, num_classes=NUM_CLASSES)
    
    opt = Adam(lr=0.01)
    model.compile(opt, loss='categorical_crossentropy', metrics=['accuracy'])
      
    ################################# Parallelize #################################

    # Distribute the neural network over multiple GPUs if available.
    gpu_count = len(available_gpus())

    if gpu_count > 1:
        print(f"\n\nModel parallelized over {gpu_count} GPUs.\n\n")
        parallel_model = keras.utils.multi_gpu_model(model, gpus=gpu_count)
    else:
        print("\n\nModel not parallelized over GPUs.\n\n")
        parallel_model = model

    parallel_model.compile(
        optimizer=opt,
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    
    ################################# Fit the model #################################

    checkpoint = keras.callbacks.ModelCheckpoint(
        '../output/chkpt_pretrain_weights.h5',
        monitor='val_acc',
        save_weights_only=True,
        save_best_only=True,
    )
    
    history = parallel_model.fit(
        x_train,
        y_train,
        validation_split = 0.1,
        batch_size=batch_size,
        epochs=epochs,
        verbose=1,
        callbacks=[checkpoint],
    )
    
    # Save the model
    model_json = parallel_model.to_json()
    with open("modified_weights.json", "w") as json_file:
        json_file.write(model_json)
    model.save('modified_weights.h5')
    
    score = parallel_model.evaluate(x_test, y_test, verbose=1, batch_size=batch_size)

    print(f'Test score:    {score[0]: .4f}')
    print(f'Test accuracy: {score[1] * 100.:.2f}')
    
    plot_acc(history)
    
    ################################ Make Predictions #################################


def build_finetune_model(base_model, num_classes):
    # Make VGG16 layers untrainable
    for layer in base_model.layers:
        layer.trainable = False
    
    # Grab VGG16 outout
    x = base_model.output
    
    # Create new softmax layer
    predictions = Dense(num_classes, activation='softmax')(x) 
    finetune_model = Model(inputs=base_model.input, outputs=predictions)
    
    return finetune_model

def available_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']

def plot_acc(history):
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig('pretrain_acc.png')
    
if __name__ == '__main__':
    main(epochs=1)