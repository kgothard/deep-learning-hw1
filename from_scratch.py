import itertools

import matplotlib.pyplot as plt
import numpy as np

from sklearn.metrics import confusion_matrix, classification_report
from sklearn.model_selection import train_test_split

from tensorflow.python.client import device_lib

import keras
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Flatten, Conv2D, MaxPool2D
from keras.layers.normalization import BatchNormalization
from keras.utils import to_categorical
from keras.optimizers import Adam

from data_prep import load_data

np.random.seed(1000)


def main(
        batch_size=128,
        epochs=1):
    
    # Load in data
    x_train, x_test, y_train, y_test = load_data()
        
    print(f'Train shape:      {x_train.shape}')
    print(f'Test shape:       {x_test.shape}')

    input1 = x_train.shape[1]
    input2 = x_train.shape[2]
    input3 = x_train.shape[3]

    # Construct the neural network
    model = build_alexnet(input1, input2, input3)
    opt = Adam(lr=0.001)   
    model.compile(
        optimizer=opt,
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )
    model.summary()

    # Distribute the neural network over multiple GPUs if available.
#    gpu_count = len(available_gpus())
    gpu_count = 1

    if gpu_count > 1:
        print(f"\n\nModel parallelized over {gpu_count} GPUs.\n\n")
        parallel_model = keras.utils.multi_gpu_model(model, gpus=gpu_count)
    else:
        print("\n\nModel not parallelized over GPUs.\n\n")
        parallel_model = model

    parallel_model.compile(
        optimizer=opt,
        loss='categorical_crossentropy',
        metrics=['accuracy'],
    )

    checkpoint = keras.callbacks.ModelCheckpoint(
        '../output/weights.h5',
        monitor='val_acc',
        save_weights_only=True,
        save_best_only=True,
    )

    parallel_model.fit(
        x_train,
        y_train,
        batch_size=batch_size,
        epochs=epochs,
        verbose=1,
        callbacks=[checkpoint],
    )

    parallel_model.load_weights('../output/weights.h5')
    score = parallel_model.evaluate(x_test, y_test, verbose=1, batch_size=batch_size)
    print(f'Test score:    {score[0]: .4f}')
    print(f'Test accuracy: {score[1] * 100.:.2f}')

    preds = parallel_model.predict(x_test, batch_size=batch_size)

    c = confusion_matrix(np.argmax(y_test, axis=-1), np.argmax(preds, axis=-1))
    plot_confusion_matrix(
        c,
        list(range(10)),
        normalize=False,
        output_path='../output',
    )
    plot_confusion_matrix(
        c,
        list(range(10)),
        normalize=True,
        output_path='../output',
    )

    print(
        classification_report(
            np.argmax(y_test, axis=-1),
            np.argmax(preds, axis=-1),
            target_names=[str(x) for x in range(10)],
        )
    )

def build_alexnet(input1, input2, input3):
    model = Sequential()

    # 1st Convolutional Layer
    model.add(Conv2D(filters=96, input_shape=(input1,input2,input3), kernel_size=(11,11), strides=(4,4), padding="valid", activation = "relu"))
    
    # Max Pooling
    model.add(MaxPool2D(pool_size=(3,3), strides=(2,2), padding="valid"))
    
    # 2nd Convolutional Layer
    model.add(Conv2D(filters=256, kernel_size=(5,5), strides=(1,1), padding="same", activation = "relu"))
    
    # Max Pooling
    model.add(MaxPool2D(pool_size=(3,3), strides=(2,2), padding="valid"))
    
    # 3rd Convolutional Layer
    model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding="same", activation = "relu"))
    
    # 4th Convolutional Layer
    model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding="same", activation = "relu"))
    
    # 5th Convolutional Layer
    model.add(Conv2D(filters=256, kernel_size=(3,3), strides=(1,1), padding="same", activation = "relu"))
    
    # Max Pooling
    model.add(MaxPool2D(pool_size=(3,3), strides=(2,2), padding="valid"))
    
    # Passing it to a Fully Connected layer
    model.add(Flatten())
    # 1st Fully Connected Layer
    model.add(Dense(units = 9216, activation = "relu"))
    
    # 2nd Fully Connected Layer
    model.add(Dense(units = 4096, activation = "relu"))
    
    # 3rd Fully Connected Layer
    model.add(Dense(4096, activation = "relu"))
    
    # Output Layer
    model.add(Dense(10, activation = "softmax")) #As we have two classes
    
    return model

def available_gpus():
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == 'GPU']


def plot_confusion_matrix(
        cm,
        classes,
        normalize=False,
        title='Confusion matrix',
        cmap='Blues',
        output_path='.',
):
    """
    Logs and plots a confusion matrix, e.g. text and image output.

    Adapted from:
        http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        tag = '_norm'
        print("Normalized confusion matrix:")
    else:
        tag = ''
        print('Confusion matrix:')
    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=plt.get_cmap(cmap))
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(
            j,
            i,
            format(cm[i, j], fmt),
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black",
        )
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.savefig(f'{output_path}/confusion{tag}.png')
    plt.close()


if __name__ == '__main__':
    main(epochs=4)
